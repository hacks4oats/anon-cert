# Anon

A small docker utility that creates throwaway self-signed certificates.

## How to use

You can use the following example to build and generate a set of self-signed certificates.

```shell
$ docker build -t anon:latest -f Dockerfile .

# You can also use the -ecdsa flag if you would like to use an ecdsa signature.
$ docker run --rm -it --name anon -v "$PWD:/home/anon" anon example.com

$ ls *.pem
private-key.pem public-key.pem  rootCA-key.pem  rootCA.pem 
```

If you wanna bundle the certificates you may do like so:

```shell
cat rootCA.pem public-key.pem >> chain.pem
```
